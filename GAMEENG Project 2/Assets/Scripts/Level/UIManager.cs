using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public new static UIManager Instance;

    public Text keyText;

    // Start is called before the first frame update
    void Start()
    {
        if (!Instance)
            Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetKeyText(int amount)
    {
        keyText.text = ("Keys: " + amount.ToString());
    }
}
