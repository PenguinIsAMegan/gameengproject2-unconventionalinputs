using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] 
    private int requiredKeys;
    PlayerData playerData;

    // Start is called before the first frame update
    void Start()
    {
        playerData = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerData>();

        if (playerData)
            playerData.EvtKeyCollected.AddListener(UnlockGate);
    }

    void OnDestroy()
    {

        if(playerData)
            playerData.EvtKeyCollected.RemoveListener(UnlockGate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            int PlayerKeys = collision.collider.GetComponent<PlayerData>().GetKeyInInventory();

            if (PlayerKeys >= requiredKeys)
                Destroy(this.gameObject);
        }
    }

    private void UnlockGate(int PlayerKeys)
    {
        if (PlayerKeys >= requiredKeys)
            Destroy(this.gameObject);
    }
}
