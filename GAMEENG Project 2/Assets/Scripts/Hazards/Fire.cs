using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public GameObject boundingBox;

    private MeshCollider box;
    private bool isTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        box = boundingBox.GetComponent<MeshCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTriggered && MicInput.Instance.isBlowing)
        {
            //Destroy(this.gameObject);
            isTriggered = false;

            foreach(Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            isTriggered = true;
            SpawnSmoke();
        }

    }

    private void SpawnSmoke()
    {
       if (isTriggered)
       {
            float posX = Random.Range(box.bounds.min.x, box.bounds.max.x);
            float posY = Random.Range(box.bounds.min.y, box.bounds.max.y);

            Vector2 pos = new Vector2(posX, posY);

            GameObject smoke = ObjectPooler.instance.SpawnFromPool("Smoke", pos, Quaternion.identity);

            smoke.transform.SetParent(this.transform);

            StartCoroutine(TimerToSpawnSmoke());
       }
    }

    public IEnumerator TimerToSpawnSmoke()
    {
        yield return new WaitForSeconds(3f);
        
        SpawnSmoke();
    }
}
