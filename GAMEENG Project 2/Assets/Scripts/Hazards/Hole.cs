using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    public bool isWinHole;

    public string nextScene;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Debug.Log("Hole Trigger");

            if (isWinHole)
            {
                collision.gameObject.GetComponent<PlayerData>().Win();
                SceneChanger.Instance.changeScene(nextScene);
            }
            else
                collision.gameObject.GetComponent<PlayerData>().FallDeath();
        }
    }
}
