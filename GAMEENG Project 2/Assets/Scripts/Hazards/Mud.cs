using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mud : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("Mud Trigger");
            collision.gameObject.GetComponent<PlayerData>().SetMudEffect(true);
        }
    }
}
