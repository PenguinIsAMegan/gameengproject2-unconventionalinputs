using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeInput : MonoBehaviour
{
    #region SingleTon

    public static ShakeInput Instance { set; get; }

    #endregion

    //Reference: https://www.youtube.com/watch?v=CPGZZUjTMhU
    public float ShakeDetectionThreshold;
    public float MinShakeInterval;

    private float sqrShakeDetectionThreshold;
    private float timeSinceLastShake;

    public bool isShakingDevice;

    void Start()
    {
        Instance = this;
        sqrShakeDetectionThreshold = Mathf.Pow(ShakeDetectionThreshold, 2);
    }

    void Update()
    {
        if (Input.acceleration.sqrMagnitude >= sqrShakeDetectionThreshold
                   && Time.unscaledTime >= timeSinceLastShake + MinShakeInterval)
        {
            isShakingDevice = true;
            timeSinceLastShake = Time.unscaledTime;
        }
        else
            isShakingDevice = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("Mud Trigger");
            //collision.gameObject.GetComponent<PlayerData>().win();
        }
    }
}
