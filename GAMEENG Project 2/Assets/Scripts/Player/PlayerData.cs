using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class keyCollected : UnityEvent<int> { }

public class PlayerData : MonoBehaviour
{
    public keyCollected EvtKeyCollected= new keyCollected();

    [SerializeField]
    private Animator animator;

    public bool isDead, hasWon, hasMud;

    [SerializeField]
    private float normGravityScale = 9.81f;
    [SerializeField]
    private float mudGravityScale = 2.0f;

    public float currentGravityScale;

    public GameObject winText;

    private Color originalColor;

    [SerializeField]
    private Color mudColor;

    private int keyInInventory;

    void Start()
    {
        currentGravityScale = normGravityScale;
        originalColor = this.GetComponent<SpriteRenderer>().material.color;
        keyInInventory = 0;
        UIManager.Instance.SetKeyText(keyInInventory);
    }

    void Update()
    {
        if (hasMud)
        {
            if (ShakeInput.Instance.isShakingDevice)
                SetMudEffect(false);
        }
    }

    public void FallDeath()
    {
        isDead = true;
        Fall();
        StartCoroutine(RestartLevel());
    }

    public void Win()
    {
        hasWon = true;
        Fall();
        winText.SetActive(hasWon);
        //StartCoroutine(RestartLevel());
    }

    void Fall()
    {
        PlayerTiltMovement playerTiltMovement = this.GetComponent<PlayerTiltMovement>();
        playerTiltMovement.ResetGravityAndVelocity();
        playerTiltMovement.isTiltLocked = true;
        animator.SetBool("isFalling", true);
    }

    IEnumerator RestartLevel()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SetMudEffect(bool setter)
    {
        SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();

        if (setter)
        {
            //Add a visual effect here
            this.GetComponent<PlayerTiltMovement>().ResetGravityAndVelocity();
            spriteRenderer.material.color = mudColor;
            currentGravityScale = mudGravityScale;
            hasMud = true;
        }
        else 
        {
            //Remove visual effect here
            spriteRenderer.material.color = originalColor;
            currentGravityScale = normGravityScale;
            hasMud = false;
        }
    }

    public void ReceiveKey()
    {
        keyInInventory++;
        UIManager.Instance.SetKeyText(keyInInventory);
        EvtKeyCollected.Invoke(keyInInventory);
    }

    public int GetKeyInInventory()
    {
        return keyInInventory;
    }
}
