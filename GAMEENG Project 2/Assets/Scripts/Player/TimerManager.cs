using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerManager : MonoBehaviour
{
    public float startTime;
    public float currentTime;

    public GameObject timeTextGameObject;
    private Text timeText;

    // Start is called before the first frame update
    void Start()
    {
        timeText = timeTextGameObject.GetComponent<Text>();

        currentTime = startTime;
        timeText.text = "Time: " + currentTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTime <= 0)
        {
            StartCoroutine(RestartLevel());
        }
        else 
        {
            currentTime -= Time.deltaTime;

            timeText.text = "Time: " + Mathf.Round(currentTime);
        }
    }

    //Probably move this to a singleton scene loader?
    IEnumerator RestartLevel()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
