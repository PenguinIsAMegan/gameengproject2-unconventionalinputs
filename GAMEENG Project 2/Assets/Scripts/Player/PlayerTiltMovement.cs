using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTiltMovement : MonoBehaviour
{
    [SerializeField] 
    float speed;
    [SerializeField]
    private PlayerData playerData;

    private Rigidbody2D rb2d;
    private Vector2 dir;

    public bool isTiltLocked;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = this.GetComponent<Rigidbody2D>();
        playerData = this.GetComponent<PlayerData>();
    }

    void Update()
    {
        //AccelerationTilt()
        if (!isTiltLocked)
            GravityTilt();
    }

    // Update is called once per frame
    //void FixedUpdate()
    //{
    //    Move();
    //}

    private void Move()
    {
        rb2d.velocity = new Vector2(dir.x * speed, dir.y * speed);
    }

    //Seems to work more realistically
    private void GravityTilt()
    {
        Physics2D.gravity = new Vector2(Input.acceleration.x * playerData.currentGravityScale, Input.acceleration.y * playerData.currentGravityScale);
    }

    public void ResetGravityAndVelocity()
    {
        Physics2D.gravity = new Vector2(0.0f, 0.0f);
        rb2d.velocity = new Vector2(0.0f, 0.0f);
    }

    private void AccelerationTilt()
    {
        //dir.x = Input.acceleration.x;
        //dir.y = Input.acceleration.y;
    }
}
